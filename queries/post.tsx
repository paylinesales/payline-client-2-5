import { gql } from "@apollo/client";

const postQuery = gql`
  query postQuery {
    posts(first: 10) {
      edges {
        node {
          title
          slug
          date
          featuredImage {
            node {
              mediaItemUrl
            }
          }
        }
      }
    }
  }
`;

export default postQuery;
