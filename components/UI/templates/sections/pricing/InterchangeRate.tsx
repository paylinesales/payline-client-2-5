/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
// import { useRouter } from "next/router";
import Image from "next/image";
import { Accordion } from "@/components/UI/atoms/Accordions/Accordion";

interface InterchangeProps {
  // eslint-disable-next-line react/require-default-props
  interchangeData?: React.ReactNode | any;
}

const InterchangeRate: React.FC<InterchangeProps> = ({ interchangeData }) => {
  const parsedData = JSON.parse(interchangeData);
  const interchangeRate =
    parsedData?.data?.pages?.edges[0].node.interchangeRate;
  const [clickedCard, setClickedCard] = useState<string>("VisaCard");
  const {
    cardAccordion: { accordion },
    creditCardLogos,
    interchangeImage1,
    interchangeImage2,
  } = interchangeRate;
  const { cardLabelRepeater: cardDetails } = accordion;

  const isActiveCard = (cardName: string) => {
    return clickedCard === cardName;
  };

  const getClickedCardDetails = () => {
    return cardDetails.find((card) => card.cardLabel === clickedCard);
  };

  const currentCard = getClickedCardDetails();
  const { cardTypeRepeater: cardTypeData } = currentCard;

  const { visa, discover, americanExpress, mastercard } = creditCardLogos;

  const handleClickedCard = (cardName: string) => () => {
    setClickedCard(cardName);
  };

  return (
    <div
      id="interchangeRate"
      className="text-center bg-interchange-background ">
      <div className="contentWrapper block md:flex md:flex-col justify-center items-center py-16 md:py-6">
        <div className="my-6 flex flex-row justify-center uppercase text-sm">
          Transparent Pricing
        </div>
        <div className="md:flex-row md:items-center justify-center mb-20 text-center text-4xl font-hkgrotesk leading-tight font-bold ">
          <span className="bg-payline-blue text-payline-white mr-2 px-2">
            Interchange
          </span>
          <span className="bg-transparent text-payline-black text-bold">
            Rates
          </span>
        </div>
        <div className="container px-5">
          <div className=" w-80 md:w-4x44 lg:w-5x44 m-auto rounded-lg flex flex-col justify-center md:bg-payline-white md:border md:border-payline-border-light">
            <div className="hidden md:flex justify-between items-center w-3/4 m-auto py-11">
              <div
                className="cursor-pointer"
                onClick={handleClickedCard("VisaCard")}>
                <Image
                  className={`${
                    isActiveCard("VisaCard") ? "opacity-100" : "opacity-50"
                  } hover:opacity-100`}
                  src={visa?.sourceUrl}
                  id="VisaCard"
                  alt="Visa Logo"
                  height="112"
                  width="112"
                  objectFit="fill"
                  layout="fixed"
                  quality={100}
                />
              </div>
              <div
                className="cursor-pointer"
                onClick={handleClickedCard("MasterCard")}>
                <Image
                  className={`${
                    isActiveCard("MasterCard") ? "opacity-100" : "opacity-50"
                  } hover:opacity-100`}
                  id="MasterCard"
                  src={mastercard?.sourceUrl}
                  alt="MasterCard Logo"
                  height="60"
                  width="77"
                  objectFit="fill"
                  quality={100}
                  layout="fixed"
                />
              </div>
              <div
                className="cursor-pointer"
                onClick={handleClickedCard("DiscoverCard")}>
                <Image
                  className={`${
                    isActiveCard("DiscoverCard") ? "opacity-100" : "opacity-50"
                  } hover:opacity-100`}
                  id="DiscoverCard"
                  src={discover?.sourceUrl}
                  alt="Discover Card Logo"
                  height="169"
                  width="169"
                  objectFit="fill"
                  quality={100}
                  layout="fixed"
                />
              </div>
              <div
                className="justify-self-end cursor-pointer"
                onClick={handleClickedCard("AMEXCard")}>
                <Image
                  className={`${
                    isActiveCard("AMEXCard") ? "opacity-100" : "opacity-50"
                  } hover:opacity-100 justify-self-end`}
                  src={americanExpress?.sourceUrl}
                  alt="American Express Logo"
                  id="AMEXCard"
                  height="71"
                  width="71"
                  objectFit="fill"
                  quality={100}
                  layout="fixed"
                />
              </div>
            </div>
            {/* Display Accordion and Images */}
            {/* Accordion Items and The accordion it's self are separate items.
          The accordion should take in the items to create as a property. */}
            <div className="flex flex-col md:flex-row w-80 md:w-4x44 lg:w-5x44">
              <div className="flex flex-col w-80 md:w-96 lg:w-3x44">
                {cardTypeData.map(({ cardType, cardTypeDetails }) => (
                  <Accordion
                    key={cardType}
                    title={cardType}
                    content={cardTypeDetails}
                    className="mb-6"
                  />
                ))}
              </div>
              <div className="hidden md:flex md:flex-col items-center w-96">
                <div className="pb-6">
                  <Image
                    src={interchangeImage1?.sourceUrl}
                    alt="Level one data"
                    height={142}
                    width={133}
                    objectFit="fill"
                    quality={100}
                    layout="fixed"
                  />
                </div>
                <div className="rounded-lg bg-payline-white border border-payline-border-dark mb-7 px-20 pt-8 pb-12">
                  <Image
                    src={interchangeImage2?.sourceUrl}
                    alt="Level 3 DATA"
                    height={111}
                    width={103}
                    objectFit="fill"
                    quality={100}
                    layout="fixed"
                  />
                  <p className="text-2xl font-arial leading-tight text-center font-bold text-payline-black">
                    Level 3 DATA
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InterchangeRate;
