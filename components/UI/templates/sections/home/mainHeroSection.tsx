import React from "react";
import { Page_Herosection as heroSectionTypes } from "@/generated/apolloComponents";

interface Props {
  data: heroSectionTypes;
}

const MainHeroSection = ({ data }: Props): React.ReactElement => {
  const { backgroundImage, headline } = data;

  const { leadingText } = headline!;
  const { highlightedText } = headline!;
  return (
    <>
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src={backgroundImage?.sourceUrl}
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                {leadingText}
                <br />
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {highlightedText}
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default MainHeroSection;
