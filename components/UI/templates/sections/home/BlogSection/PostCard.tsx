/* eslint-disable no-console */
import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";

const dateFormatter = (unformattedDate: string) => {
  const date = new Date(unformattedDate);
  const monthName = date.toLocaleString("en-US", {
    month: "long",
  });
  return `${date.getDate()} ${monthName} ${date.getFullYear()}`;
};

const PostCard: React.FC<{
  imgSrc: string;
  unformattedDate: string;
  title: string;
  slug: string;
}> = (props) => {
  const { imgSrc, unformattedDate, title, slug } = props;
  const postDate = dateFormatter(unformattedDate);
  const router = useRouter();
  useEffect(() => {
    router.prefetch(`/blog/${slug}`);
  }, []);

  const onClick = (e) => {
    e.preventDefault();
    console.log("Pre-Fetching Link");
    router.push(`/blog/${slug}`);
  };
  return (
    <>
      {console.log(imgSrc)}
      {imgSrc !== "undefined-150x150undefined" || "" ? (
        <img src={imgSrc} className="w-full object-cover" />
      ) : null}
      <a href={`/blog/${slug}`} onClick={onClick}>
        <div className="postcard-content px-6 pb-8 pt-3">
          <p className="uppercase text-payline-dark">{postDate}</p>

          <h4 className="font-semibold text-payline-black hover:underline text-xl mt-2">
            <a>{title}</a>
          </h4>

          <a className="mt-8 font-bold text-payline-black text-2xl hover:underline flex gap-8 items-center pl-3">
            Read
            <FontAwesomeIcon icon={faAngleRight} />
          </a>
        </div>
      </a>
    </>
  );
};

export default PostCard;
